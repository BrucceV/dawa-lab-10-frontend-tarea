import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

/* import GoogleSignInButton from 'vue-google-signin-button-directive' */
import GoogleAuth from 'vue-google-oauth'
//Vue.use(GoogleAuth, { client_id: '473202198889-k3nb2gsofsgao20jciat4ljr1tusoto8.apps.googleusercontent.com' })
Vue.use(GoogleAuth, { client_id: '473202198889-ddbfmdtfr8cnmiojt5lnhe57tnnhftgq.apps.googleusercontent.com' })
Vue.googleAuth().load()

import BootstrapVue from 'bootstrap-vue'
Vue.use(BootstrapVue)

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import axios from 'axios'
import VueAxios from 'vue-axios'

Vue.use(VueAxios, axios)

axios.defaults.baseURL = 'https://brucce-lab10-tarea.herokuapp.com/';
//axios.defaults.baseURL = 'http://localhost:3000/';

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')